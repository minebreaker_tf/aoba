var gulp = require('gulp');
var ts = require('gulp-typescript');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var minify = require('gulp-minify');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var rimraf = require('rimraf');
var replace = require('gulp-replace');

gulp.task('clean', function (callback) {
    return rimraf('./build', callback);
});

gulp.task('copy-html', function () {
    gulp.src(['./src/*.html'])
        .pipe(gulp.dest('build'));
    gulp.src(['./src/css/*.css'])
        .pipe(gulp.dest('build/css'));
    gulp.src(['./sample/*'])
        .pipe(gulp.dest('build/content/'));
    gulp.src(['./sample/subdir/*'])
        .pipe(gulp.dest('build/content/subdir'));
});

gulp.task('compile', function () {
    var tsProject = ts.createProject('tsconfig.json');

    return tsProject.src()
        .pipe(tsProject()).js
        .pipe(gulp.dest('build/temp'));
});

gulp.task('browserify', ['compile'], function () {
    return browserify({
        basedir: '.',
        debug: true,
        entries: [
            './build/temp/main.js'
        ],
        noParse: [
            require.resolve('jquery') // Bootstrapのためhtmlから読み込む
        ],
        cache: {},
        packageCache: {}
    })
        .bundle()
        .pipe(source('aoba.js'))
        .pipe(gulp.dest('build'));
});

gulp.task('minify', ['minify-js', 'minify-html', 'minify-css'], function () {
});

gulp.task('minify-js', ['browserify'], function () {
    return gulp.src('./build/aoba.js')
        .pipe(minify({
            ext: {
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('minify-html', [], function () {
    return gulp.src('src/*.html')
        .pipe(replace('aoba.js', 'aoba.min.js'))
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(gulp.dest('build'));
});

gulp.task('minify-css', [], function () {
    return gulp.src('src/css/*.css')
        .pipe(cleanCSS())
        .pipe(gulp.dest('build/css'));
});

gulp.task('env', function () {
    process.env.NODE_ENV = 'production';
});

gulp.task('dev-build', ['copy-html', 'compile', 'browserify'], function () {
});

gulp.task('build', ['env', 'compile', 'browserify', 'minify'], function () {
});

gulp.task('default', ['build'], function () {
});
