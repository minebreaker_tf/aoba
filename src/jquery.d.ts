/// <reference path="../typings/index.d.ts" />

declare module 'jquery' {

    interface jQuery {
        (onReady: Function): void;
        (selector: string): any;
        (document: Document): any;
        ajax(settings: any): void;
    }

    const $: jQuery;

    export = $;

}
