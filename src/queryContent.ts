/// <reference path="../typings/index.d.ts" />
/// <reference path="./jquery.d.ts" />

import $ = require('jquery');

function fetch(param: string, suffix: string, onSuccess: Function, onError: Function): void {

    const path = replaceAll(param, '.', '/');
    const prefix = 'content/';

    ajax(
        prefix + path + suffix,
        onSuccess,
        onError
    );

}

function ajax(url: string, onSuccess: Function, onError: Function): void {
    $.ajax({
        url: url,
        success: (res) => onSuccess(res),
        error: (res) => onError(res)
    });
}

function replaceAll(target: string, searchValue: string, replacValue): string {
    if (target.indexOf(searchValue) < 0) {
        return target;
    } else {
        return replaceAll(target.replace(searchValue, replacValue), searchValue, replacValue);
    }
}

export default fetch;
