/// <reference path="../typings/index.d.ts" />

declare module 'omnipotent/decorator/observer' {

    function observer(arg: any, arg2: any, arg3: any): any;

    export = observer;

}
