/// <reference path="../typings/index.d.ts" />

import * as React from "react";
import * as ReactDOM from "react-dom";
import App from "./components/app";
import state from "./state";
// import omniscient = require('omniscient');
import Path = require("pathjs");
import fetch from "./queryContent";
import $ = require('jquery');
import setRoutes from "./route";

//noinspection TypeScriptUnresolvedFunction
// omniscient.debug();

const pathjs = Path.pathjs;
declare const hljs: any;

setRoutes(hljs);

$(() => {

    ReactDOM.render(
        <App state={ state }/>,
        document.getElementById('aoba-main'));

    fetch(
        'settings',
        '.json',
        (content) => {
            const headerCursor = state.cursor('header');
            headerCursor.set('title', content.header.title);
            headerCursor.set('labels', content.header.labels);
            headerCursor.set('links', content.header.links);
            state.cursor('body').set('prefix', content.header.title);
            state.cursor('footer').set('content', content.footer.content);
        },
        (content) => console.log(content)
    );

    pathjs.listen();

});
