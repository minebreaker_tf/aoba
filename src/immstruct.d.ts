/// <reference path="../typings/index.d.ts" />

declare module 'immstruct' {

    function immstruct(arg: any): any;

    export = immstruct;

}