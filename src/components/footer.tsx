/// <reference path="../../typings/index.d.ts" />

import * as React from 'react';
import omniscient = require('omniscient');
import $ = require('jquery');

const Footer = omniscient(({state}) => {

    const fixed: boolean = state.get('fixed') || false;

    if (fixed) $('body').css('padding-bottom', '80px');
    else $('body').css('padding-bottom', '0');

    return <div className={ fixed ? "navbar navbar-default navbar-fixed-bottom" : "navbar navbar-default"} role="navigation">
        <div className="container">
            <p className="navbar-text">{ state.get('content') }</p>
            <p className="navbar-text navbar-right hidden-xs">
                Made with <span className="glyphicon glyphicon-heart" aria-hidden="true"/> by
                <a href="https://bitbucket.org/minebreaker_tf/aoba" target="_blank"> Aoba</a>
            </p>
        </div>
    </div>
});

export default Footer;
