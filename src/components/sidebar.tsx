/// <reference path="../../typings/index.d.ts" />

import * as React from "react";
import omniscient = require('omniscient');

const Sidebar = omniscient(({state}) => {

    const labels: Array<string> = state.get('labels');
    const links: Array<string> = state.get('links');
    //noinspection JSMismatchedCollectionQueryUpdate
    const rows: Array<any> = labels.length === links.length ?
        labels.map((obj, i) => <p key={ i }><a href={ links[i] }>{ obj }</a></p>) :
        [];

    return <div className="col-md-2 hidden-xs hidden-sm" role="navigation">
        <div className="container affix">
            { rows }
            {/*<p><a href="#/index">Top</a></p>*/}
        </div>
    </div>
});

export default Sidebar;
