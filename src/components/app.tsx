/// <reference path="../../typings/index.d.ts" />

import * as React from "react";
import Header from "./header";
import Footer from "./footer";
import Body from "./body";
import omniscient = require('omniscient');
import observer = require("omnipotent/decorator/observer");
import state from "../state";

const BodyWrapper = observer(state, {'state': ['body']}, Body);
const HeaderWrapper = observer(state, {'state': ['header']}, Header);
const FooterWrapper = observer(state, {'state': ['footer']}, Footer);

const App = omniscient(({state}) =>
    <div>
        <HeaderWrapper />
        <BodyWrapper />
        <FooterWrapper />
    </div>
);

export default App;
