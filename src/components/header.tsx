/// <reference path="../../typings/index.d.ts" />

import * as React from "react";
import omniscient = require('omniscient');

const Header = omniscient(({state}) => {

    const labels: Array<string> = state.get('labels');
    const links: Array<string> = state.get('links');
    //noinspection JSMismatchedCollectionQueryUpdate
    const rows: Array<any> = labels.length === links.length ?
        labels.map((obj, i) => <li key={ i }><a href={ links[i] }>{ obj }</a></li>) :
        [];

    return <div className="navbar navbar-default navbar-fixed-top" role="navigation">
        <div className="container">
            <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#toggle" aria-expanded="false">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"/>
                    <span className="icon-bar"/>
                    <span className="icon-bar"/>
                </button>
                <a className="navbar-brand" href="https://bitbucket.org/minebreaker_tf/aoba"
                   target="_blank">{ state.get('title') }</a>
            </div>

            <div className="collapse navbar-collapse" id="toggle">
                <ul className="nav navbar-nav navbar-right">{ rows }</ul>
            </div>
        </div>
    </div>
});

export default Header;
