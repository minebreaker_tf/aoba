/// <reference path="../../typings/index.d.ts" />
/// <reference path="../marked.d.ts" />

import * as React from "react";
import Sidebar from "./sidebar";
import omniscient = require('omniscient');
import marked = require('marked');
import $ = require("jquery");

const Body = omniscient(({state}) => {

    // const testHandler = () => {
    //     state.set('title', 'updated!');
    // };

    // bodyの描画にあわせてタイトルを更新する
    $('title').html(state.get('prefix') + ' - ' + state.get('title'));

    return <div className="container">
        <div className="row">
            <div className="col-md-10">
                <h1>{ state.get('title') }</h1>
                <div dangerouslySetInnerHTML={ {__html: marked(state.get('content') || 'error')} }/>
                {/*<button className="btn btn-default" onClick={ testHandler }>Click to test</button>*/}
            </div>
            <Sidebar state={ state.cursor('sidebar') } />
        </div>
    </div>
});

export default Body;
