/// <reference path="../typings/index.d.ts" />

declare module 'pathjs' {

    interface pathjs {
        map(arg: any): any;
        listen(): void;
        root(rootPath: string): void;
    }

    interface Path {
        pathjs: pathjs;
    }

    let Path: Path;

    export = Path;

}
