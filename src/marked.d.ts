/// <reference path="../typings/index.d.ts" />

declare module 'marked' {

    function marked(markdown: string): string;
    function marked(markdown: string, callback: Function): string;

    export = marked;

}
