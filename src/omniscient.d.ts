/// <reference path="../typings/index.d.ts" />

declare module 'omniscient' {

    type Lambda = any;

    interface ComponentCreator {
        (opt: Lambda): any
    }

    interface OptionAssigner {
        (opt: Lambda): any
    }

    function omniscient(options: OptionAssigner): ComponentCreator;
    function omniscient(options: any, op2: any): ComponentCreator;

    export = omniscient;

}
