/// <reference path="../typings/index.d.ts" />

import Path = require("pathjs");
import state from "./state";
import fetch from "./queryContent";
import $ = require('jquery');

const pathjs = Path.pathjs;

export default function setRoutes(hljs) {
    pathjs.root('#/index');
    pathjs.map('#/:content').to(function () { // ラムダで書けない
        // TODO refactoring
        const param = this.params['content'];
        const bodyCursor = state.cursor('body');
        fetch(
            param,
            '.json',
            (content) => {
                bodyCursor.set('title', content.title);
                const setLinks = (labels, links) => {
                    const sidebar = bodyCursor.cursor('sidebar');
                    sidebar.set('labels', labels);
                    sidebar.set('links', links);
                };

                if (typeof content.sidebar !== 'undefined') {
                    setLinks(content.sidebar.labels, content.sidebar.links);
                } else {
                    setLinks([], []);
                }

                if (typeof content.footer !== 'undefined' && typeof content.footer.fixed === 'boolean') {
                    state.cursor('footer').set('fixed', content.footer.fixed);
                } else {
                    state.cursor('footer').set('fixed', false);
                }
            },
            (content) => console.log(content)
        );
        fetch(
            param,
            '.md',
            (content) => {
                bodyCursor.set('content', content);

                // append classes for Bootstrap
                $('table').attr('class', 'table table-striped table-bordered');
                // highlight.js
                $('pre').each(function (i, block) {
                    hljs.highlightBlock(block);
                });
            },
            (content) => {
                bodyCursor.set('title', 'Content not found');
                bodyCursor.set('content', 'Something is wrong with the page you requested.');
            }
        );
    });
}
