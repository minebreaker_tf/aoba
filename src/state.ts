/// <reference path="../typings/index.d.ts" />

import immstruct = require("immstruct");

const state = immstruct({
    view: 'index',
    body: {
        title: 'default-body-title',
        prefix: 'default-prefix',
        content: 'default-body-content',
        sidebar: {
            labels: [],
            links: []
        }
    },
    header: {
        title: "default-title",
        labels: ["default-link"],
        links: ["#/index"]
    },
    footer: {
        content: "default-copyright",
        fixed: false
    }
});

export default state;
